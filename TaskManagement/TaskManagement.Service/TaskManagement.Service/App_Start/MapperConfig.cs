﻿using AutoMapper;
using System;
using TaskManagement.Domain.Entities.Main;

namespace TaskManagement.Service.App_Start
{
    public static class MapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(mapping =>
            {
                mapping.CreateMap<Color, Color>()
                .ForMember(d => d.Code, opt => opt.MapFrom(m => m.Code))
                .ForMember(d => d.Value, opt => opt.MapFrom(m => m.Value))
                .ForMember(d => d.EditDate, opt => opt.NullSubstitute(DateTime.Now))
                .ForMember(d => d.EditUserId, opt => opt.NullSubstitute(1))
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.RegUserId, opt => opt.Ignore())
                .ForMember(d => d.RegDate, opt => opt.Ignore());

                mapping.CreateMap<Task, Task>()
                .ForMember(d=>d.ColorId, opt=>opt.MapFrom(m=>m.ColorId))
                .ForMember(d => d.Heading, opt => opt.MapFrom(m => m.Heading))
                .ForMember(d => d.Description, opt => opt.MapFrom(m => m.Description))
                .ForMember(d => d.FromDate, opt => opt.MapFrom(m => m.FromDate))
                .ForMember(d => d.ToDate, opt => opt.MapFrom(m => m.ToDate))
                .ForMember(d => d.EditDate, opt => opt.NullSubstitute(DateTime.Now))
                .ForMember(d => d.EditUserId, opt => opt.NullSubstitute(1))
                .ForMember(d => d.Id, opt => opt.Ignore())
                .ForMember(d => d.UserId, opt => opt.Ignore())
                .ForMember(d => d.RegUserId, opt => opt.Ignore())
                .ForMember(d => d.RegDate, opt => opt.Ignore());
            });
        }
    }
}