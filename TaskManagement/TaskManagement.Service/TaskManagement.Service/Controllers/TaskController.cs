﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Web.Http;
using TaskManagement.BusinessLogic.Services.Abstraction;
using TaskManagement.DataAccess.UnitOfWork.Abstraction;
using TaskManagement.Domain.Entities.Main;

namespace TaskManagement.Service.Controllers
{
    public class TaskController : ApiController
    {
        private IUnitOfWork _unitOfWork;
        ITaskService taskService;
        private Task _task;

        public TaskController(IUnitOfWork unitOfWork, ITaskService taskService)
        {
            _unitOfWork = unitOfWork;
            this.taskService = taskService;
        }

        public IHttpActionResult GetAll(int id)
        {
            List<Task> tasks = _unitOfWork
                .GetRepository<Task>()
                .GetAll(m=>m.UserId == id);

            return Ok(tasks);
        }

        public IHttpActionResult Get(int id)
        {
            _task = _unitOfWork
                .GetRepository<Task>()
                .Get(m => m.Id == id);

            if (_task == null)
                return Content(HttpStatusCode.NoContent, "The requested resource has not been modified since the last time you accessed it!");

            return Ok(_task);
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody]Task task)
        {
            if (task == null)
                return BadRequest();

            if (string.IsNullOrEmpty(task.Heading) || string.IsNullOrEmpty(task.Description) || task.FromDate == DateTime.MinValue || task.FromDate == DateTime.MinValue)
                return Content(HttpStatusCode.BadRequest, "Some datas is empty!");

            _task = taskService.CheckTaskTimeConflict(task);

            if(_task == null)
                _unitOfWork
                    .GetRepository<Task>()
                    .Add(task);

            _unitOfWork.SaveChanges();

            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Edit(int id, [FromBody]Task task)
        {
            if (task == null)
                return BadRequest();

            if (string.IsNullOrEmpty(task.Heading) || string.IsNullOrEmpty(task.Description) || task.FromDate == DateTime.MinValue || task.FromDate == DateTime.MinValue)
                return Content(HttpStatusCode.BadRequest, "Some datas is empty!");

            var _task = _unitOfWork
                .GetRepository<Task>()
                .Get(m => m.Id == id);

            if (_task == null)
                return Content(HttpStatusCode.NoContent, "The requested resource has not been modified since the last time you accessed it!");

            Mapper.Map(task, _task);

            var checkTask = taskService.CheckTaskTimeConflict(task);
            if (checkTask == null)
            {
                _unitOfWork.SaveChanges();
                return Ok();
            }
            else
            {
                return Content(HttpStatusCode.Conflict, "");
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id < 1)
                return Content(HttpStatusCode.BadRequest, "ID value is wrong!");

            _task = _unitOfWork
                .GetRepository<Task>()
                .Get(m => m.Id == id);

            if (_task == null)
                return Content(HttpStatusCode.NoContent, "The requested resource has not been modified since the last time you accessed it!");

            try
            {
                _unitOfWork.GetRepository<Task>().Delete(_task);
                _unitOfWork.SaveChanges();
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

            return Ok();
        }
    }
}
