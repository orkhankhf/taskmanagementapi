﻿using AutoMapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Web.Http;
using TaskManagement.DataAccess.UnitOfWork.Abstraction;
using TaskManagement.Domain.Entities.Main;

namespace TaskManagement.Service.Controllers
{
    public class ColorController : ApiController
    {
        IUnitOfWork _unitOfWork;
        private Color _color;

        public ColorController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IHttpActionResult GetAll()
        {
            List<Color> colors = _unitOfWork
                .GetRepository<Color>()
                .GetAll();
            return Ok(colors);
        }

        public IHttpActionResult Get(int id)
        {
            _color = _unitOfWork
            .GetRepository<Color>()
            .Get(m => m.Id == id);

            if(_color == null)
                return Content(HttpStatusCode.NoContent, "The requested resource has not been modified since the last time you accessed it!");

            return Ok(_color);
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody]Color color)
        {
            if (color == null)
                return BadRequest();

            if (string.IsNullOrEmpty(color.Value) || string.IsNullOrEmpty(color.Code))
                return Content(HttpStatusCode.BadRequest, "Color value or code is empty!");

            _unitOfWork
                .GetRepository<Color>()
                .Add(color);

            _unitOfWork.SaveChanges();

            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Edit(int id, [FromBody]Color color)
        {
            if (color == null)
                return BadRequest();

            if (string.IsNullOrEmpty(color.Value) || string.IsNullOrEmpty(color.Code))
                return Content(HttpStatusCode.BadRequest, "Color value or code is empty!");

            var _color = _unitOfWork
                .GetRepository<Color>()
                .Get(m => m.Id == id);

            if (_color == null)
                return Content(HttpStatusCode.NoContent, "The requested resource has not been modified since the last time you accessed it!");

            Mapper.Map(color, _color);

            _unitOfWork.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id < 1)
                return Content(HttpStatusCode.BadRequest, "ID value is wrong!");
            
            _color = _unitOfWork
                .GetRepository<Color>()
                .Get(m => m.Id == id);

            if (_color == null)
                return Content(HttpStatusCode.NoContent, "The requested resource has not been modified since the last time you accessed it!");

            try
            {
                _unitOfWork.GetRepository<Color>().Delete(_color);
                _unitOfWork.SaveChanges();
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }

            return Ok();
        }
    }
}
