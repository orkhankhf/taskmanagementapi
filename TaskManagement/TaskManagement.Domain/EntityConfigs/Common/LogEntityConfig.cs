﻿using TaskManagement.Domain.Entities.Common;
using TaskManagement.Domain.EntityConfigs.Base;

namespace TaskManagement.Domain.EntityConfigs.Common
{
    public class LogEntityConfig<T> : EntityConfig<T> where T : LogEntity
    {
        public LogEntityConfig()
        {
            Property(m => m.RegUserId).HasColumnName("RegUser");
            Property(m => m.EditUserId).HasColumnName("EditUser");
            Property(m => m.ArchivedUserId).HasColumnName("ArchivedUser");
        }
    }
}
