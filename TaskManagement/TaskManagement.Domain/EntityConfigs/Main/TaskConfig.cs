﻿using TaskManagement.Domain.Entities.Main;
using TaskManagement.Domain.EntityConfigs.Common;

namespace TaskManagement.Domain.EntityConfigs.Main
{
    public class TaskConfig : LogEntityConfig<Task>
    {
        public TaskConfig()
        {
            HasRequired(m => m.Color).WithMany().HasForeignKey(m => m.ColorId);
            HasRequired(m => m.User).WithMany().HasForeignKey(m => m.UserId);
            ToTable("Tasks");
        }
    }
}
