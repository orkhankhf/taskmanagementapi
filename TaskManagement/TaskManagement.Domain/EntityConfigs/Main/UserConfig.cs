﻿using TaskManagement.Domain.Entities.Main;
using TaskManagement.Domain.EntityConfigs.Common;

namespace TaskManagement.Domain.EntityConfigs.Main
{
    public class UserConfig : LogEntityConfig<User>
    {
        public UserConfig()
        {
            ToTable("Users");
        }
    }
}
