﻿using TaskManagement.Domain.Entities.Main;
using TaskManagement.Domain.EntityConfigs.Common;

namespace TaskManagement.Domain.EntityConfigs.Main
{
    public class ColorConfig : LogEntityConfig<Color>
    {
        public ColorConfig()
        {
            ToTable("Colors");
        }
    }
}
