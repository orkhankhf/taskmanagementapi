﻿using System.Data.Entity.ModelConfiguration;
using TaskManagement.Domain.Entities.Base;

namespace TaskManagement.Domain.EntityConfigs.Base
{
    public class EntityConfig<T> : EntityTypeConfiguration<T> where T : Entity
    {
        public EntityConfig()
        {
            HasKey(m => m.Id);
            Property(m => m.Id).HasColumnName("Id");
        }
    }
}
