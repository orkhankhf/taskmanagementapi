﻿using TaskManagement.Domain.Entities.Common;

namespace TaskManagement.Domain.Entities.Main
{
    public class User : LogEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
