﻿using System;
using System.ComponentModel.DataAnnotations;
using TaskManagement.Domain.Entities.Common;
using FluentValidation.Attributes;
using TaskManagement.Domain.EntityValidations.Main;

namespace TaskManagement.Domain.Entities.Main
{
    [Validator(typeof(TaskValidator))]
    public class Task : LogEntity
    {
        public string Heading { get; set; }
        public string Description { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int ColorId { get; set; }
        public Color Color { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
