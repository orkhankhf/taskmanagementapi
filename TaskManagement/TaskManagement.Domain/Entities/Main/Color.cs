﻿using FluentValidation.Attributes;
using TaskManagement.Domain.Entities.Common;
using TaskManagement.Domain.EntityValidations.Main;

namespace TaskManagement.Domain.Entities.Main
{
    [Validator(typeof(ColorValidator))]
    public class Color : LogEntity
    {
        public string Value { get; set; }
        public string Code { get; set; }
    }
}
