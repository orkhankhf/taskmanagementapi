﻿using System;
using TaskManagement.Domain.Entities.Base;

namespace TaskManagement.Domain.Entities.Common
{
    public abstract class LogEntity : Entity
    {
        public int RegUserId { get; set; } = 1;
        public DateTime RegDate { get; set; } = DateTime.Now;
        public int? EditUserId { get; set; }
        public DateTime? EditDate { get; set; }
        public int? ArchivedUserId { get; set; }
    }
}
