﻿using FluentValidation;
using TaskManagement.Domain.Entities.Main;

namespace TaskManagement.Domain.EntityValidations.Main
{
    public class ColorValidator : AbstractValidator<Color>
    {
        public ColorValidator()
        {
            RuleFor(m => m.Code)
                .NotEmpty()
                .MaximumLength(10);

            RuleFor(m => m.Value)
                .NotEmpty()
                .MaximumLength(25);
        }
    }
}
