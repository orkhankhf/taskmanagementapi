﻿using FluentValidation;
using TaskManagement.Domain.Entities.Main;

namespace TaskManagement.Domain.EntityValidations.Main
{
    public class TaskValidator : AbstractValidator<Task>
    {
        public TaskValidator()
        {
            RuleFor(m => m.ColorId)
                .NotNull()
                .GreaterThan(0);
            
            RuleFor(m => m.Heading)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(m => m.Description)
                .MaximumLength(500);

            RuleFor(m => m.FromDate)
                .NotEmpty()
                .LessThan(m=>m.ToDate);

            RuleFor(m => m.FromDate)
                .NotEmpty();
        }
    }
}
