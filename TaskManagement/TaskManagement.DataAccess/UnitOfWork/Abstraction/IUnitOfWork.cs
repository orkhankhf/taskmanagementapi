﻿using System;
using TaskManagement.DataAccess.Repositories.Abstraction;

namespace TaskManagement.DataAccess.UnitOfWork.Abstraction
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<T> GetRepository<T>() where T : class;
        int SaveChanges();
    }
}
