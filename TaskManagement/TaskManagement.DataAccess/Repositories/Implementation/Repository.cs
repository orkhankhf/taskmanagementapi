﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using TaskManagement.DataAccess.DbContexts;
using TaskManagement.DataAccess.Repositories.Abstraction;

namespace TaskManagement.DataAccess.Repositories.Implementation
{
    class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly TaskContext Context;

        DbSet<TEntity> dbSet;

        public Repository(TaskContext _entities)
        {
            Context = _entities;
            dbSet = Context.Set<TEntity>();
        }

        public List<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate = null)
        {
            if (predicate != null)
            {
                return  dbSet.Where(predicate).ToList();
            }

            return dbSet.ToList();
        }

        public TEntity Get(Expression<Func<TEntity, bool>> predicate)
        {
            return dbSet.FirstOrDefault(predicate);
        }

        public void Add(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            dbSet.Remove(entity);
        }

        public IQueryable<TEntity> QuerybleData()
        {
            IQueryable<TEntity> query = dbSet;
            return query;
        }
    }
}
