﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace TaskManagement.DataAccess.DbContexts
{
    public class TaskContext : DbContext
    {
        public TaskContext() : base("name=TaskContext")
        {
            Database.SetInitializer<TaskContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");

            var assembly = Assembly.Load("TaskManagement.Domain");

            var mappingClasses = assembly.GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract &&
                !string.IsNullOrWhiteSpace(t.Name) &&
                t.Name.EndsWith("Config"))
                .ToList();

            foreach (var mappingClass in mappingClasses)
            {
                dynamic mappingInstance = Activator.CreateInstance(mappingClass);
                modelBuilder.Configurations.Add(mappingInstance);
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
