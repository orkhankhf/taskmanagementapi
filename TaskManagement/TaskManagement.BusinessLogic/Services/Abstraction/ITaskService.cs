﻿using TaskManagement.Domain.Entities.Main;

namespace TaskManagement.BusinessLogic.Services.Abstraction
{
    public interface ITaskService
    {
        Task CheckTaskTimeConflict(Task task);
    }
}
