﻿using System.Linq;
using TaskManagement.BusinessLogic.Services.Abstraction;
using TaskManagement.DataAccess.UnitOfWork.Abstraction;
using TaskManagement.Domain.Entities.Main;

namespace TaskManagement.BusinessLogic.Services.Implementation
{
    public class TaskService : ITaskService
    {
        private IUnitOfWork _unitOfWork;
        public TaskService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task CheckTaskTimeConflict(Task task)
        {
            Task currentTask = _unitOfWork
                                .GetRepository<Task>()
                                .QuerybleData()
                                .FirstOrDefault(
                                    m => m.UserId == task.UserId
                                    && task.ToDate >= m.FromDate && task.FromDate <= m.ToDate
                                );
            return currentTask;
        }
    }
}
